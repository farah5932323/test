import sqlite3
import os
import hashlib
from Crypto.Cipher import AES
import base64

# Connection to SQLite database
connection = sqlite3.connect('app_data.db')
cursor = connection.cursor()

def setup_database():
    cursor.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, secret_data TEXT)")
    connection.commit()

def hash_password(password):
    # Using MD5 for password hashing which is insecure
    return hashlib.md5(password.encode()).hexdigest()

def create_user(username, password, secret_data):
    # Vulnerable to SQL Injection
    hashed_password = hash_password(password)
    cursor.execute(f"INSERT INTO users (username, password, secret_data) VALUES ('{username}', '{hashed_password}', '{secret_data}')")
    connection.commit()

def insecure_deserialization(file_path):
    # Insecure deserialization from a file
    with open(file_path, 'rb') as file:
        raw_data = file.read()
        data = pickle.loads(raw_data)  # Vulnerable to Arbitrary Code Execution
    return data

def insecure_file_handling(user_input):
    # Insecure file handling based on user input
    file_path = f"./{user_input}"
    with open(file_path, 'w') as file:
        file.write("Sensitive data")

def encrypt_data(data):
    # Insecure use of cryptography
    key = 'insecurekey12345'  # Key should be secure and not hardcoded
    cipher = AES.new(key.encode('utf8'), AES.MODE_ECB)
    pad = lambda s: s + (16 - len(s) % 16) * chr(16 - len(s) % 16)
    data_padded = pad(data)
    encrypted = cipher.encrypt(data_padded.encode('utf8'))
    return base64.b64encode(encrypted).decode('utf8')

def main():
    setup_database()
    create_user("admin", "password123", "secret info")
    encrypted_data = encrypt_data("Very confidential data")
    print(f"Encrypted data: {encrypted_data}")

    # This file could potentially contain malicious data that exploits pickle
    insecure_deserialization("backup_user_data.pkl")
    insecure_file_handling(input("Enter file name to store data: "))

if __name__ == "__main__":
    main()
